from shell_utils import *
from datausage_utils import *
import re
from datetime import datetime

# Returns Master Total Data Usage
def write_mastertotal_datausage (rootpaths, outfile, now, hostname):
    # Reporting raw df
    outfile.write(hostname)
    outfile.write('\n\n')
    outfile.write(runshell_nosplit('df -h | grep -E "(Filesystem|/$)"'))
    outfile.write('{:-<56} '.format('')+'\n')

    # Total disk used data
    totalusage = int(runshell_nosplit(r"""df | grep -E "\/$" | awk '{print $3}'"""))
    totalusagegb = totalusage / 1048576
    outfile.write('Total Data Usage  : '+'{:.0f}'.format(totalusagegb)+' G\n')

    # Total project related used data
    argfortotal = ""
    for rootpath in rootpaths:
        if (rootpath[3]): argfortotal = argfortotal + rootpath[0] + ' ';

    datausage = get_datausage(0, 'kM', '-c '+argfortotal)

    for line in datausage.splitlines():
        if(re.search('.*total.*', line)):
            totalprjusage = int(re.sub('^(\d{1,})\s{1,}G.*total.*', r"\1", line))

    # Reporting 'overhead', 'non-project-related', 'system related', 'unknown' data usage
    outfile.write('Project Data Usage: '+'{:d}'.format(totalprjusage)+' G ('+argfortotal+')\n')
    unknownusage = totalusagegb-totalprjusage
    unknownusageperc = "{:.0%}".format(unknownusage/totalusagegb)
    outfile.write('Unknown Data Usage: '+'{:.0f}'.format(unknownusage)+' G ('+unknownusageperc+')\n')

    # Reporting project data usage
    outfile.write(colorize_old_data(datausage, now))
    outfile.write('{:-<56} '.format('')+'\n')

# Returns data usage of major (root paths) with Depth 1
def write_rootpaths_datausage (rootpath, outfile, now):
    cwd = os.getcwd()
    os.chdir(rootpath)
    outfile.write(rootpath+'\n')
    outfile.write(colorize_old_data(get_datausage(1, 'kM', ''), now))
    outfile.write('{:-<56} '.format('')+'\n')
    os.chdir(cwd)

# Writes detailed, colorized by idleness, with configurable depth and recursion
def write_detailed_datausage (rootpath, depth, recursion, outfile, now):
    if (depth != 0):
        cwd = os.getcwd()
        os.chdir(rootpath)
        if(not recursion):
            outfile.write('{:-<56} '.format(rootpath+' ')+'\n')
            subpaths = runshell('find . -maxdepth 1 -type d | grep -v "^\.$"')
            for path in subpaths:
                os.chdir(path)
                lines = get_datausage(depth, 'kM', '')
                if (lines != ""):
                    outfile.write(path+'\n')
                    colorized_lines = colorize_old_data(lines, now)
                    outfile.write(colorized_lines)
                    outfile.write('\n')
                os.chdir(rootpath)
        else:
            write_detailed_datausage(os.getcwd(), depth, False, outfile, now)
        os.chdir(cwd)

# Identifies old data and html-marks it to highlight to user/owner
def colorize_old_data(lines, now):
    colorized_lines = ''
    for line in lines.splitlines():
        size            = re.sub('(.*)\t(\d{4}-\d{2}-\d{2})\t(.*)', r"\1", line)
        lastwrittendate = re.sub('(.*)\t(\d{4}-\d{2}-\d{2})\t(.*)', r"\2", line)
        path            = re.sub('(.*)\t(\d{4}-\d{2}-\d{2})\t(.*)', r"\3", line)
        date = datetime.strptime(lastwrittendate, "%Y-%m-%d")
        diff = now-date
        months = diff.days//30
        if(months > 6):
            #  --------------------------------------------------------     (maximum size mobile gmail: 56 chars)
            #  size-- ----path---------------------------  idlenessmsg-
            idlenessmsg = r"""Idle: <mark>"""+str(months)+r""" mon</mark>"""
            aux = (size, path, idlenessmsg)
            line = '{0:6} {1:35} {2:>12}'.format(*aux)
        elif(months >= 1):
            idlenessmsg = r"""Idle: """+str(months)+r""" mon"""
            aux = (size, path, idlenessmsg)
            line = '{0:6} {1:35} {2:>12}'.format(*aux)
        else:
            idlenessmsg = ''
            aux = (size, path, idlenessmsg)
            line = '{0:6} {1:35} {2:>12}'.format(*aux)
        colorized_lines = colorized_lines + line + '\n'
    return colorized_lines

