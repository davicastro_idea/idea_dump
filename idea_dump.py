#!/Tools/anaconda/install/bin/python
from shell_utils import *
from servers_config import *
from datausage_functions import *
from sendmail_utils import *
from datetime import datetime

# datetime object containing current date and time
now = datetime.now()
dt_string = now.strftime("%Y%m%d_%H%M%S")   # dd/mm/YY H:M:S
dt_string2 = now.strftime("%Y/%m/%d %H:%M")
print("date and time =", dt_string)

# System Info
pwd = os.getcwd()
hostname = runshell('hostname')
hostname = hostname[0]
rootdiskusagepercent = runshell(r"""df -h | grep -E "\/$" | awk '{print $5}'""")
rootdiskusagepercent = rootdiskusagepercent[0]

# Email Body File:
outf = '/tmp/idea_dump_'+dt_string+'.txt'
fout = open(outf, 'w')

# Initial Total for meaningful root paths
write_mastertotal_datausage(rootpaths[hostname], fout, now, hostname)

# Simple High Level Data Usage for the Rootpaths provided
for rootpath in rootpaths[hostname]:
    write_rootpaths_datausage(rootpath[0], fout, now)

# In depth Data Usage for Rootpaths provided (possibly with recursion inside)
for rootpath in rootpaths[hostname]:
    write_detailed_datausage(rootpath[0], rootpath[1], rootpath[2], fout, now)

# Wrap-up and sendmail
fout.close()
os.chdir(pwd)

subject = "idea_D.U.M.P.: *"+hostname+'* has '+rootdiskusagepercent+' disk used ('+dt_string2+')'
emaillist = get_users_email(outf)
sendmailhtml('htmlheader.txt'+' '+outf+' '+'htmlfooter.txt', subject, emaillist, 'Cezar Voltarelli <cezar.voltarelli@idea-ip.com>,Sara Brito <sara.brito@idea-ip.com>, Davi Castro <davi.castro@idea-ip.com>, Daniel Formiga <daniel.formiga@idea-ip.com>, Carlos Castro <carlos.castro@idea-ip.com>, Tomaz Vilela <tomaz.vilela@idea-ip.com>, Andreia <andreia.vassao@idea-ip.com>', '')
