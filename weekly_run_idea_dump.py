#!/Tools/anaconda/install/bin/python
import time
from shell_utils import *

while (True):
    runshell('./idea_dump.py')
    #  oneweekinseconds = 7*24*60*60
    twodaysinseconds = 2*24*60*60
    print('Sleeping for {:d} seconds...'.format(twodaysinseconds))
    time.sleep(twodaysinseconds)

