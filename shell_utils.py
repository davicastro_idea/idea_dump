#!/usr/bin/python3.6
import subprocess
import os

# Execute bash command and splilines result
def runshell (bashcmd):
    result = subprocess.run(bashcmd, shell=True, stdout=subprocess.PIPE)
    result = result.stdout.decode('utf-8')
    result = result.splitlines();
    return result

# Execute bash command
def runshell_nosplit (bashcmd):
    result = subprocess.run(bashcmd, shell=True, stdout=subprocess.PIPE)
    result = result.stdout.decode('utf-8')
    return result

