# Idea!'s D.U.M.P
**D**ata

**U**sage

**M**onitor and

**P**urge

Creates data usage report in pre-defined directories and sends an email to all users of the server.

### What needs to be setup?

* Configure paths to be analyzed per server (hostname) in the file: *servers_config.py*
* Configure usernames and email in file: *users_config.py*
    * All contributions to keep those two files up-to-date are much welcome!
* Tested with Python: Python 3.6.5 :: Anaconda, Inc
    * In Idea!'s server you need to execute Tool configuration script: load\_titan, load\_galaxy

### How to execute?

* Just call ./idea\_dump.py

### Features

* Calculates Unknown data (which would be /home, or the system files) and how much % this is of total disk usage
    * Idea is this could revel we have too much overhead of system, or we need to add more paths to be analyzed
* Uses *du --time* as a criteria to distinguish idle directories based on last written date
* Uses ionice to not disturb other processes in execution: this also means it takes long time to create the report
* Uses HTML markdown format to highlight very old data
* Detects usernames based on format "user.name" (a dot in the middle)

### Limitations

* Symlinks are not supported! They are not followed at all.
    * If du is called with follow-symlinks enabled (-L I think), it could double count symlinks to pdk data (like inside Innovus' database)
* It relies on simple Linux utilies: du, ionice, sort, awk
    * Python is just a "wrapper" to execute du
    * This means Python is not 'database aware', it just parses few info to highlight some old data
    * More advanced stuff like sending messages to specific users, and many other, would require a re-work of the code

### Future Features:

* Calculate for each user the hidden .nfs$#!@%!@ files that resides within git-annex directory (Detected by Sara)
    * This could be easily deleted to improve disk space.
    * This is probably failed rsync transfer that kept these files
* Send to each user of really old data the ssh command to purge the data (don't know if this is a good idea)

