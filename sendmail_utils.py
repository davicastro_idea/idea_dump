import re
from shell_utils import *
from users_config import *

# Sendmail that works with HTML
def sendmailhtml (bodyfile, subject, to, cc, bcc):
    cmd = ('( echo "To: ' + to + '"; echo "Cc: ' + cc + '"; echo "Bcc: ' + bcc + '"; echo "Subject: ' + subject +
          '"; echo "From: noreply@idea-ip.com"; echo "Content-Type: text/html"; echo; cat ' +
          bodyfile + '; echo; ) | sendmail -t')
    runshell(cmd)

def get_users_email (bodyfile):
    userlist = runshell('cat '+bodyfile+' | grep -E -o "[a-z]{1,}\.[a-z]{1,}" | sort -u')
    emaillist = ''
    for user in userlist:
        try:
            emaillist = emaillist + users[user] + ', '
        except:
            print('Unknown user: '+user)
    return re.sub(', $','',emaillist)

