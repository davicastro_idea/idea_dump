from shell_utils import *

# Raw shell commands to report IONICEly Data Usage
ionice           = 'ionice -c 2 -n 7 '
du_time          = ionice+'du --time '
#  du_totalsummary  = du_time+'-c -s '
def du(depth):
    return du_time+'--max-depth='+str(depth)+' '

# Raw shell commands to reverse sort (bigger before smaller) than humanize (k, M, G sizes)
reverse_sort_humanize = r"""sort -r -n | awk '{split("k M G",v); s=1; while($1>1024 && s<3){$1/=1024; s++} print int($1)" "v[s]"\t"$2"\t"$4}' """

# Function to report data usage in human way, bigger to smaller, filtering k or k and M bytes
grepout_k  = ' | grep -v -E "\sk\s"'
grepout_kM = ' | grep -v -E "\s(k|M)\s"'
def get_datausage(depth, sizefilter, args):
    if(sizefilter == 'kM'):
        grepstring = grepout_kM
    elif(sizefilter == 'k'):
        grepstring = grepout_k
    else:
        grepstring = ''
    return runshell_nosplit(du(depth) + args + ' | ' + reverse_sort_humanize + grepstring)

#  hadouken = r"""ionice -c 2 -n 7 du --max-depth=1 --time | sort -r -n | awk '{split("k M G",v); s=1; while($1>1024 && s<3){$1/=1024; s++} print int($1)" "v[s]"\t"$2"\t"$4}' | """ + grepout_k
#  ionicedu = r"""ionice -c 2 -n 7 du --max-depth=1"""

def custom_hadouken (depth, grepout):
    return r"""ionice -c 2 -n 7 du --max-depth=""" + str(depth) + r""" --time | sort -r -n | awk '{split("k M G",v); s=1; while($1>1024 && s<3){$1/=1024; s++} print int($1)" "v[s]"\t"$2"\t"$4}'""" + grepout

def custom_hadouken_root():
    #  return r"""ionice -c 2 -n 7 du -c -s -D --time $(ls -1 | paste -s | sed 's/\t/ /g') | sort -r -n | awk '{split("k M G",v); s=1; while($1>1024 && s<3){$1/=1024; s++} print int($1)" "v[s]"\t"$2"\t"$4}'""" + grepout_kM
    return r"""ionice -c 2 -n 7 du -c -s -D --time $(find . -maxdepth 1 -type d -or -type l | grep -v "^\.$ | paste -s | sed 's/\t/ /g') | sort -r -n | awk '{split("k M G",v); s=1; while($1>1024 && s<3){$1/=1024; s++} print int($1)" "v[s]"\t"$2"\t"$4}'""" + grepout_kM

def custom_hadouken_root_args(args):
    return r"""ionice -c 2 -n 7 du -c -s -D --time """ + args + r""" | sort -r -n | awk '{split("k M G",v); s=1; while($1>1024 && s<3){$1/=1024; s++} print int($1)" "v[s]"\t"$2"\t"$4}'""" + grepout_kM

